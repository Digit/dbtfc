Digit's Big Tiny Font Collection Checklist
==========================================

checking the state of each font.

- is it scalable or pixel.
- does it at least have £ or more incomplete
- is it pragmatic or whimsy


is it scalable or pixel.
------------------------

scalable
--------

bf.ttf  
bggf.ttf  
clso.ttf  
djfl.ttf  
djth-8.ttf  
jffh.ttf  
jffhle.ttf  
jffhlo.ttf  
jffhlor.ttf  
jffhlu.ttf  
kob.ttf  
mer.ttf  
merg.ttf  
mergag.ttf  
mergy.ttf  
mnky.ttf  
nzt.ttf  
nzt_originalbrkn_backup.ttf  
nzt_originalfixed_backup.ttf  
nztr.ttf  
nzts.ttf  
nztt.ttf  
rmnd.ttf  
tidyr.ttf  
tidyra.ttf  
wastage.ttf  
wastest.ttf  
yz.ttf  
zia.ttf  

pixel
-----

brf.otb  
carpy.otb  
feral.otb  
lem.otb  
mo.otb  
pe.otb  
pen.otb  
procrastination.otb  
qnr.otb  
referal.otb  
tiptoe.otb  
tydls.otb  
zfr.otb  

does it at least have £ or more incomplete
-------------------------------------------

please note, these are all minimal character set.  none are "complete".  just enough to get by, for most uses.  the more exotic characters are not in any of them.  so sorry if you wanted umlats, chinese characters and so on

quids in
--------

bf \`¬~£  
brf \`¬~£  
carpy \`~£  
feral & referal \`~£  
jffhle \`~£  
jffhlor \`~£  
jffhlu \`~£  
jffhlor (broken spaces) \`~£  
lem \`~£  
mo \`¬~£  
nzt nztr nzts (broken spaces) \`~£  
nztt \`~£  
pe pen \`~£  
rmnd \`~£  
tiptoe \`~£  


incomplete
----------

a  
ablnn  
ablns  
ac  
aci  
bggf  
clso  
djfl  
djsl  
jffh  
jffhlo  
kob  
mer merg mergog mergy  
mnky  
procrastination  
qnr  
tidyr tidyrbdfri (broken)  
tydls  
was wash wastage wastest  
yz  
zfr  

pragmatic or whimsy
-------------------

many of these are both.  hard to gauge which camp to put many of them in.  some are clearly more whimsy than pragmatic.  some, it's a matter of opinion and resolution.  in my opinion, the tiny and rly tiny fonts are pragmatic, but i've separated them out here for convenience.

pragmatic
---------

bggf  
carpy (can help with dyslexia, weighted)  
clso  
djsl  
jffhlu & jffhle (and incomplete versions)  
mergy (and precursor versions)  
mnky  
mo (if you dont mind the morse above)  
nztt (and prior broken versions)  
procrastination  
qnr  
was wash wastage wastest (square)  
yz  

whimsy
------

a, ac & aci (just a bit too cartoon wiggly)  
djfl (that e... knocks it out of pragmatic)  
kob (like klingon or something)  
lem (stylish)  
mo (morse does above... is that pragmatic?)  


rly tiny
--------

bf (the tiniest!  b\*\*\*\*\*\* f\*\*\*!)  
brf  
qnub (uhh... where's qnub gone?)  
zfr  

tiny
----

feral & referal  
nztt (scalable)  
pe & pen  
rmnd  
tydls  
tiptoe  
yz (scalable)  

full list of exported, regardless if really incomplete and broken, fixed pixel and scalable
=================================================================


bf.ttf
bggf.ttf
brf.otb
carpy.otb
clso.ttf
djfl.ttf
djth-8.ttf
feral.otb
jffh.ttf
jffhle.ttf
jffhlo.ttf
jffhlor.ttf
jffhlu.ttf
kob.ttf
lem.otb
mer.ttf
merg.ttf
mergag.ttf
mergy.ttf
mnky.ttf
mo.otb
nzt.ttf
nzt_originalbrkn_backup.ttf
nzt_originalfixed_backup.ttf
nztr.ttf
nzts.ttf
nztt.ttf
pe.otb
pen.otb
procrastination.otb
qnr.otb
referal.otb
rmnd.ttf
tidyr.ttf
tidyra.ttf
tiptoe.otb
tydls.otb
wastage.ttf
wastest.ttf
yz.ttf
zfr.otb
zia.ttf



full list, including un-exported and really incomplete and broken
=================================================================

a
ablnn
ablns
ac
aci
acil
aimhi
aimhiexp
bf
bggf
binki
binkibu
brf
brfci
brfcv
buns
carpy
cbrn
clj
cljh
cljy
clso
cnub
crspr
cslo
delg-altlowercasessss
delg
djfk
djfl
djth
djtqmbl
elvbot
emi
feral
fff
forgy
frtv
gmln
gmry
gompu
gompua
grg
grgi
herbykit
jffh
jffhle
jffhlo
jffhlor
jrld-1-bkp
jrld-blankTemplate9x6Template
jrld
jrldtesta
kob
koba
krb
krbo
lem
lnki
mer
mergy
mnkh
mnki
mo
murk
nkl
nty
nzt
nzt_originalbrkn_backup
nztr
nztr~
nzts
nzts~
nztt
pcf
pe
pen
per
procrastination
qnr
rgl
rgld
rgr
rmd
rmnd
rnkl
rnkle
rnklh
rnkli
rnklj
rnklr
rnklt
rnklw
rnklx
rnkly
rnklz
scab
sixdee
svndee
tidyd
tidyrb
tidyrbdfr
tidyrbdfri
tidyrbdfriz
tidyrbdfrizx
tidys
tiptoe
tnky
tnu
tnui
tnuid
tnuidi
tnuidig
trd
tydl
tydls
was
wash
wast
wastage
yz
zfr
zia
zot
zotii
zotiin


so what's good
==============

Digit's faves
-------------


| font name      | scalable     | quids in     | pragmatic    | monospace   | score \*    |
| -------------- | :----------: | :----------: | :----------: | :---------: | ----------: |
|  merg          | **yes**      | no           | yes          | yes (tall)  | 2.5         |
|  nztt          | **yes**      | **yes**      | yes tiny     | yes         | **5**       |
|  mnky          | no 7         | no           | yes          | yes         | 2           |
|  bf            | no 4         | **yes**      | rly tiny     | yes         | 3           |
|  qnub-test     | no 4         | yes          | yes (lost)   | yes         | 3           | refound!
|  yz            | **yes**      | no           | yes tiny     | yes         | 4           |
|  djfl          | **yes**      | no           | midway (e)   | yes         | 2.5         |
|  rmnd          | no           | **yes**      | yes tiny     | yes         | 4-(spacing) |
|  tydls         | no           | no           | yes tiny     | yes         | 3           |
|  brf           | no           | **yes**      | rly tiny     | yes         | 3           |
|  zfr           | no           | no           | rly tiny     | yes         | 2           |
|  referal       | no           | **yes**      | yes tiny     | yes         | 4           |
|  pen           | no           | **yes**      | yes tiny     | yes         | 4           |
|  tiptoe        | no           | **yes**      | yes tiny     | yes         | 4           |
|  jffhlu/jffhle | **yes**      | **yes**      | yes tiny     | yes         | **5**       |
|  bggf          | **yes**      | no           | yes tinyish  | yes         | 3.5         |
|  djsl          | no 6         | no           | yes tiny     | yes         | 3           |
|  ~~zia~~       | no 8         | idk          | yes          | yes         | ~~0~~(lost) | refound! broken!
|  carpy         | no 6         | **yes**      | yes tiny(+d) | yes         | 4.5         |
|  djsl          | no 6         | no           | yes tiny     | yes         | 3           |


\* scoring: 1 point for scalable, quids in, & monospace.  1 for pragmatic + 1 for tiny, just 1 for rly tiny.  minor tweaks per case (see carpy & djfl's pragmatic, & merg's tall).  scores do not reflect aesthetic preference (whereby, e.g. zfr & mnky rank higher than the score of 2 implies).


Digit's emacs fonts
-------------------

still true as of 20190619

```lisp
;;bring back yz somewhere in the mix.  yz is nice.  no?
(setq font-zoom-list
      (list "-*-merg-*-*-*-*-16-*-*-*-*-*-*-*" ;;CONFIRMEDFONT
	    "-*-merg-*-*-*-*-24-*-*-*-*-*-*-*" ;;testing~fence
	    "-*-merg-*-*-*-*-32-*-*-*-*-*-*-*" ;;CONFIRMEDFONT
	    ;;	    "-*-yz-*-*-*-*-5-*-*-*-*-*-*-*" ;;prior list faves, but broken
	    "-*-fixed-*-*-*-*-10-*-*-*-*-*-*-*"
	    "-*-mnky-*-*-*-*-6-*-*-*-*-*-*-*" ;;blocks
;;	    "-*-djfl-*-*-*-*-7-*-*-*-*-*-*-*" ;; nice, but uncertain.  works well at 72
;;	    "-*-rmnd-*-*-*-*-9-*-*-*-*-*-*-*" ;; otb gets picked up over ttf
	    "-*-tydls-*-*-*-*-8-*-*-*-*-*-*-*" ;; tydls "tidals"/"tiddles" an old fave... but otb.
            "-*-bf-*-*-*-*-4-*-*-*-*-*-*-*" ;; the smallest
;;            "-*-'qnub-test'-*-*-*-*-4-*-*-*-*-*-*-*" ;; quantum sized
            "-*-tydls-*-*-*-*-6-*-*-*-*-*-*-*" ;;
            "-*-nztt-*-*-*-*-6-*-*-*-*-*-*-*" ;;"((t (:height 42 :family "nzt")))"
	    "-*-brf-*-*-*-*-5-*-*-*-*-*-*-*" ;; tight club
	    "-*-zfr-*-*-*-*-5-*-*-*-*-*-*-*" ;; tight club
	    "-*-referal-*-*-*-*-5-*-*-*-*-*-*-*" ;; another fave, tight club
	    "-*-pen-*-*-*-*-5-*-*-*-*-*-*-*" ;; tight club
	    "-*-tydls-*-*-*-*-5-*-*-*-*-*-*-*" ;; remake of classic tidyr, tight club
;;	    "-*-nztt-*-*-*-*-5.5-*-*-*-*-*-*-*" ;; rar
	    "-*-nztt-*-*-*-*-6-*-*-*-*-*-*-*" ;; my fave (scalable)
;; nice, but off for flow	    "-*-yz-*-*-*-*-7-*-*-*-*-*-*-*"  ;;"((t (:height 49 :family "yz")))"
;; nice, but off for flow	    "-*-tiptoe-*-*-*-*-6-*-*-*-*-*-*-*"
;;    	    "-*-jffhlo-*-*-*-*-6-*-*-*-*-*-*-*" why was this one not on?
	    "-*-djsl-*-*-*-*-7-*-*-*-*-*-*-*" ;; my old fave (non-scalable)
;;    	    "-*-jffhlo-*-*-*-*-7-*-*-*-*-*-*-*" 
;;	    ;;	    "-*-zia-*-*-*-*-8-*-*-*-*-*-*-*"  ;;"((t (:height ?? :family "zia")))"    ;;  why does this not work in my devuan?
	    "-*-mnky-*-*-*-*-12-*-*-*-*-*-*-*" ;;testing
	    "-*-merg-*-*-*-*-16-*-*-*-*-*-*-*" ;;CONFIRMEDFONT
	    "-*-merg-*-*-*-*-32-*-*-*-*-*-*-*" ;;
	    "-*-carpy-*-*-*-*-10-*-*-*-*-*-*-*" ;;testing, anti-dyslexia
	    "-*-nztt-*-*-*-*-6-*-*-*-*-*-*-*" ;; my fave (scalable)
;;  	    "-*-jffhlo-*-*-*-*-8-*-*-*-*-*-*-*" ;;retesting
  	    "-*-jffhle-*-*-*-*-8-*-*-*-*-*-*-*" ;;retesting	    
	    "-*-nztt-*-*-*-*-12-*-*-*-*-*-*-*" ;; my fave (scalable)
;;	    "-*-jffhlo-*-*-*-*-12-*-*-*-*-*-*-*" ;;retesting
;;	    "-*-jffhlo-*-*-*-*-16-*-*-*-*-*-*-*" ;;retesting
	    "-*-jffhle-*-*-*-*-12-*-*-*-*-*-*-*" ;;retesting
	    "-*-jffhle-*-*-*-*-16-*-*-*-*-*-*-*" ;;retesting
;;	    "-*-nztt-*-*-*-*-8-*-*-*-*-*-*-*" ;; my fave (scalable)
;;	    "-*-nztt-*-*-*-*-9-*-*-*-*-*-*-*" ;; my fave (scalable)
	    "-*-nztt-*-*-*-*-18-*-*-*-*-*-*-*" ;; my fave (scalable)
;;	    "-*-jffhlo-*-*-*-*-24-*-*-*-*-*-*-*" ;;retesting
	    "-*-jffhle-*-*-*-*-24-*-*-*-*-*-*-*" ;;retesting
	    "-*-nztt-*-*-*-*-24-*-*-*-*-*-*-*" ;; my fave (scalable)
	    "-*-merg-*-*-*-*-32-*-*-*-*-*-*-*" ;;	    
	    "-*-nztt-*-*-*-*-32-*-*-*-*-*-*-*" ;; my fave (scalable)
;;    	    "-*-jffhlo-*-*-*-*-32-*-*-*-*-*-*-*" ;;
    	    "-*-jffhle-*-*-*-*-32-*-*-*-*-*-*-*" ;;
	    "-*-merg-*-*-*-*-64-*-*-*-*-*-*-*" ;;CONFIRMEDFONT	    
	    "-*-nztt-*-*-*-*-64-*-*-*-*-*-*-*" ;; my fave (scalable)
;;    	    "-*-jffhlo-*-*-*-*-64-*-*-*-*-*-*-*" ;; jffhlor & jffhlu too, btw.  but the hle version has copyright and registered copyright too.  :3  progress.  :)
    	    "-*-jffhle-*-*-*-*-64-*-*-*-*-*-*-*" ;;
    	    "-*-merg-*-*-*-*-128-*-*-*-*-*-*-*" ;;CONFIRMEDFONT
	    ))
```
