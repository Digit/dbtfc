Digit's Big Tiny Font Collection
================================

a big (growing) collection of (mostly) tiny fonts.


to scale or not to scale, that is the precision.
------------------------------------------------

most are bitmap fonts for xwindows, not designed for scaling, but for pixel perfection.

some are now also better suited to scaling.


missing characters
------------------

most are just a basic set of characters.  some have more characters filled out.  if you like a font, but it is lacking some special characters you use, you are welcome to request Digit (via contact means at bottom of this readme) continue to make such characters.  only on some of the very smallest fonts would this not be possible.

alternatively, you can of course try make them yourself.  fire up fontforge, open the relevant .sfd file, and see what you can do.  do it well, n consider sharing your modifications/additions.

mmk, how can I check them out simply?
-------------------------------------

idk, can you download them directly from  https://notabug.org/Digit/dbtfc/src/master/fonts ?

or...

```git glone "https://notabug.org/digit/dbtfc" ; cp dbtfc/fonts/*ttf ~/.fonts/ ; cp dbtfc/fonts/*otb ~/.fonts/ # and make some program use them.```


past snapshot releases
----------------------

### Digit's Big Tiny Font Collection:
http://gnome-look.org/content/show.php?content=166224

### Digits Big Tiny Font Collection 2:
http://box-look.org/content/show.php?content=174580

### and the old rolling dir:
*this NotABug.org git repo will be a better curated version of*
http://ks392457.kimsufi.com/stuff/fonts/

plan
----
i'll:

* clean up the broken      
* remove the flotsam ✓
* remove stuff i didnt make ✓ (i think ~ will continually recheck that)
* update/improve the fonts
* rearrange them better \*
* make & add example images
* etc. (?)
and maybe even
* fonts in own repos
* plan out of README.md \*
* make scripts to help automate creation of preview images (inc 3 line example that shows vert spacing)
* etc. (?)

\* started organizing in checklist.md

contact
-------

Digit's usually available via irc in #peers or #witchlinux on irc.libera.chat , or
(most reliably) via diaspora ~~digit@joindiaspora.com~~ digit@iviv.hu (for now).


mmk, how can I check them out simply?
-------------------------------------

idk, can you download them directly from  https://notabug.org/Digit/dbtfc/src/master/fonts ?

 or...

 ```git glone "https://notabug.org/digit/dbtfc" ; cp dbtfc/fonts/*ttf ~/.fonts/ ; cp dbtfc/fonts/*otb ~/.fonts/ # and make some program use them.```



